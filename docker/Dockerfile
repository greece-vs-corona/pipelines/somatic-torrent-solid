FROM iontorrent/tsbuild AS builder
ARG TVC_VERSION=5.12.1
RUN curl -sL https://github.com/iontorrent/TS/archive/TorrentSuite_${TVC_VERSION}.tar.gz |\
    tar xzf - --strip-components 1 -C /src && \
    cd /src && /usr/local/bin/buildpkg.sh gpu Analysis

FROM ubuntu:bionic 
WORKDIR /tmp
# Update the repository sources list
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y \
    perl \
    python2.7 \
    libgfortran3 \
    libatlas-base-dev liblapack-dev libblas-dev \
    python-pandas

RUN /usr/bin/python -V || ln -s /usr/bin/python2.7 /usr/bin/python

RUN mkdir -p /opt/coverageAnalysis
WORKDIR /opt/coverageAnalysis
COPY --from=builder /src/plugin/coverageAnalysis/bed bed
COPY --from=builder /src/plugin/coverageAnalysis/scripts scripts
COPY --from=builder /src/plugin/coverageAnalysis/run_coverage_analysis.sh .

RUN apt-get install -y \
        r-base r-base-dev
RUN /usr/bin/R --vanilla -e 'install.packages("stringr", repos="https://cloud.r-project.org")'
ENV PATH=$PATH:/opt

WORKDIR /usr/local/bin
COPY coverage_report.sh /opt
COPY --from=builder /src/plugin/variantCaller/bin/*.py /usr/local/bin/
COPY --from=builder /src/build/Analysis/*.deb ion-analysis.deb
#COPY --from=builder /src/build/gpu/*.deb ion-gpu.deb
RUN dpkg -i ion-analysis.deb && rm *.deb && apt-get clean

