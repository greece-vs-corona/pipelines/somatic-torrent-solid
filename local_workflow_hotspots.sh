#!/bin/bash

if [[ $# -ne 5 ]]; then
    echo "Usage: `basename $0` <ref-fasta> <parameters-file> <targets-bed> <input-ubam> <hotspots-bed>"
    exit 1
fi


HGREF="$1"
PARAMS_FILE="$2"
TARGETS_BED="$3"
INPUT_BAM="$4"
HOTSPOTS_BED="$5"

THREADS=12

# Try to get the Sample name from the BAM file:
PREFIX=Sample_$(samtools view -H "$INPUT_BAM" |fgrep @RG | fgrep SM:| head | tr '\t' '\n' | fgrep SM: | sed -e 's/SM://' -e 's|[ /:$#]|_|g' | uniq)
OUTDIR="./tvc-out/$PREFIX"
mkdir -p "$OUTDIR"

ALIGNED_BAM="$OUTDIR/${PREFIX}_aligned.bam"
SORTED_BAM="$OUTDIR/${PREFIX}_aligned_sorted.bam"

# The folowing commands (and especially their parameters) have been extracted
# from logs of runs in the Torrent Suite and documentation foujnd in the Thermo
# Fisher website and the web. They should be valid for DNA Ampliseq workflows,
# and more specifically the Oncomine Comprehensive Assay v3 and Colon and Lung
# designs.

BED_FNAME=$(basename "$TARGETS_BED" | sed 's/\.[^.]*$//')
PTRIM_BED="$OUTDIR/${BED_FNAME}_unmerged_detail.bed"
PLAIN_BED="$OUTDIR/${BED_FNAME}_merged_plain.bed"

echo Preraring merged (plain) target BED file 
tvcutils validate_bed \
	--target-regions-bed "$TARGETS_BED" \
	--reference "$HGREF" \
	--unmerged-detail-bed "$PTRIM_BED" \
	--merged-plain-bed "$PLAIN_BED" || exit $?


HOTSPOTS_FNAME=$(basename "$HOTSPOTS_BED" | sed 's/\.[^.]*$//')
HOTSPOTS_LEFT_BED="$OUTDIR/${HOTSPOTS_FNAME}_left_aligned.bed"
HOTSPOTS_VCF="$OUTDIR/${HOTSPOTS_FNAME}.vcf"

echo Preparing Hotspots VCF
tvcutils prepare_hotspots \
	--input-bed "$HOTSPOTS_BED" \
	--reference "$HGREF" \
	--left-alignment on  --allow-block-substitutions on \
	--output-bed "$HOTSPOTS_LEFT_BED" \
	--output-vcf "$HOTSPOTS_VCF" \
	--unmerged-bed "$PTRIM_BED" || exit $?

echo Running tmap, output will be stored in "$OUTDIR"
tmap mapall -J 25 --end-repair 15 --do-repeat-clip --context \
	-u -v --prefix-exclude 5 -Y \
	-r "$INPUT_BAM" -f "$HGREF" -o 2 -n $THREADS \
	-i bam -s "$ALIGNED_BAM" stage1 map4 || exit $?

echo Sorting aligned BAM "$ALIGNED_BAM"
samtools sort -@ $THREADS -o "$SORTED_BAM" -O bam \
	-T `basename "$ALIGNED_BAM"` "$ALIGNED_BAM" || exit $?

echo Indexing sorted BAM "$SORTED_BAM"
samtools index -b "$SORTED_BAM" || exit $?

echo Running variant caller...
variant_caller_pipeline.py -o "$OUTDIR" \
    --num-threads $THREADS \
    --region-bed "$PLAIN_BED" \
    --primer-trim-bed "$PTRIM_BED" \
    --hotspot-vcf "$HOTSPOTS_VCF" \
    --input-bam "$SORTED_BAM" --parameters-file "$PARAMS_FILE" \
    --reference-fasta "$HGREF" || exit $?
