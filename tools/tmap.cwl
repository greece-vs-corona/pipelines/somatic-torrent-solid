class: CommandLineTool
cwlVersion: v1.0
$namespaces:
  sbg: 'https://www.sevenbridges.com/'
id: tmap
baseCommand:
  - tmap
  - mapall
inputs:
  - id: input_ubam
    type: File
    inputBinding:
      position: 0
      prefix: '-r'
    label: Unaligned BAM
    doc: Input unaligned BAM file
  - id: reference
    type: File
    inputBinding:
      position: 0
      prefix: '-f'
    label: Reference genome
    doc: FASTA file containing reference genome
    secondaryFiles:
      - .fai
      - .tmap.anno
      - .tmap.bwt
      - .tmap.pac
      - .tmap.sa
outputs:
  - id: aligned_bam
    doc: The output aligned BAM file
    label: Aligned BAM
    type: File
    outputBinding:
      glob: $(inputs.input_ubam.nameroot)_aligned.bam
label: tmap
arguments:
  - position: 1
    prefix: '-o'
    valueFrom: '2'
  - position: 1
    prefix: '-q'
    valueFrom: '50000'
  - position: 1
    prefix: '-n'
    valueFrom: '24'
  - position: 1
    prefix: '-i'
    valueFrom: bam
  - position: 6
    prefix: ''
    separate: false
    shellQuote: false
    valueFrom: stage1
  - position: 7
    prefix: ''
    separate: false
    shellQuote: false
    valueFrom: map4
  - position: 1
    prefix: '-s'
    valueFrom: $(inputs.input_ubam.nameroot)_aligned.bam
requirements:
  - class: ShellCommandRequirement
  - class: InlineJavascriptRequirement
hints:
  - class: DockerRequirement
    dockerPull: sgsfak/tmap-tvc
